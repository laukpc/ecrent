//
//  ViewController.swift
//  eCRent
//
//  Created by Laurynas Kapacinskas on 2020-10-06.
//

import UIKit


class WelcomeScreenController: UIViewController, NetworkingClientDelegate {

    

    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var imageLogo: UIImageView!
    
    // go to main viewController
    @IBAction func continuePressed(_ sender: Any) {
        performSegue(withIdentifier: "segueToMain", sender: self)
        
    }
    
    var networkingClient = NetworkingClient()
    var data : [CarModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueButton.layer.cornerRadius = 3
        imageLogo.layer.cornerRadius = imageLogo.frame.size.height/2
        // fetch data here
        networkingClient.networkingDelegate = self
        networkingClient.fetchCarData()
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // transfer data with segue to main viewcontroller
        if segue.identifier == "segueToMain" {
            let destinationVC = segue.destination as! MainViewController
            destinationVC.carsArray = data
        }
    }
    
    func fetchCarData(_ networkingClient: NetworkingClient, cars: [CarModel]) {
        data = cars
    }
    
    func didFailWithError(error: Error) {
        
    }

}

