//
//  mainViewController.swift
//  eCRent
//
//  Created by Laurynas Kapacinskas on 2020-10-07.
//

import UIKit
import AlamofireImage
import Alamofire
import CoreLocation

class MainViewController: UIViewController {
    
    var carsArray : [CarModel]?
    var currentCoordinate : CLLocation?
    
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func filterByBattery(_ sender: Any) {
        
        carsArray = carsArray?.sorted(by:  {$0.batteryPercentage > $1.batteryPercentage})
        tableView.reloadData()
    }
    
    @IBAction func filterByLocation(_ sender: Any) {
        
        let alert = UIAlertController(title: "Premium feature", message: "Ooops..this feature is premium, to activate it - you need to buy it 😬", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK, I will transfer 💶", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
        filterByDistance()
    }
    
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "CustomTableViewCell")
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
    }
    
    func filterByDistance() {
        
        /* if i had distance data in [carModel] it would be easy, but to have it there it would be reasonable to put the distance calculation when parsing JSON and adding to the carModel object. But in this case I haven't managed to filter it
         */
        
        print("😩")
        
        //carsArray = carsArray?.sorted(by:  {$0.distance > $1.distance})
        //reload data in tableview after sorting
        //tableView.reloadData()
        
    }
}


//MARK: TableView Ext


extension MainViewController : UITableViewDataSource,  UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return carsArray?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // registering a cell
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as! CustomTableViewCell
        
        // adding data
        
        cell.carModelName.text =  carsArray?[indexPath.row].carModel ?? "Unknown"
        
        cell.battery.text = String("Battery \(carsArray![indexPath.row].batteryPercentage)%")
        
        let url = carsArray?[indexPath.row].photo
        
        //downloading photo and setting it
        if let imageURL = URL(string: url!), let placeholder = UIImage(named: "load-icon-png-7948") {
            cell.buttonImageView.af
                .setImage(withURL: imageURL, placeholderImage: placeholder)
        }
        
        //  calculating distance and setting it
        
        let carLat = carsArray?[indexPath.row].latitude
        let carLon = carsArray?[indexPath.row].longitude
        
        if let carLat = carLat, let carLon = carLon {
            
            let carCoordinate  = CLLocation(latitude: carLat, longitude: carLon)
            if let currentCoordinate = currentCoordinate  {
                let distanceToCarInt =  calculateDistance(currentLocation: currentCoordinate, carCoordinate: carCoordinate)
                let distanceToCarString = String("\(distanceToCarInt) KM")
                cell.distance.text = distanceToCarString
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //deselecting row for cleaner look
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

//MARK: CLLoc ext

extension MainViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location =  locations.last {
            locationManager.stopUpdatingLocation()
            let lat = location.coordinate.latitude
            let lon = location.coordinate.longitude
            currentCoordinate = CLLocation(latitude: lat, longitude: lon)
            //reloading data after setting current location to update it instantly
            tableView.reloadData()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func calculateDistance ( currentLocation : CLLocation, carCoordinate  : CLLocation) -> Int {
        // distance is in meters so we convert it to km
        let distance = Int(carCoordinate.distance(from: currentLocation)/1000)
        
        return distance
    }
    
}
