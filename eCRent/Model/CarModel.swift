//
//  carModel.swift
//  eCRent
//
//  Created by Laurynas Kapacinskas on 2020-10-07.
//

import UIKit

// model that will be used to populate car array

struct CarModel {
    
    let carModel : String
    let plateNumber : String
    let batteryPercentage : Int
    let photo : String
    let latitude : Double
    let longitude : Double
    var distance : Int?
    
    
    init(carModel  : String, plateNumber : String, batteryPercentage : Int, photo : String, latitude : Double, longitude : Double  ) {
        
        self.carModel = carModel
        self.plateNumber  = plateNumber
        self.batteryPercentage = batteryPercentage
        self.photo = photo
        self.latitude = latitude
        self.longitude = longitude
    }

}
