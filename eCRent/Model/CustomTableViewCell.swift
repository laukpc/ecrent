//
//  CustomTableViewCell.swift
//  eCRent
//
//  Created by Laurynas Kapacinskas on 2020-10-07.
//  Copyright © 2020 Laurynas Kapacinskas. All rights reserved.
//
import UIKit
import SnapKit

class CustomTableViewCell: UITableViewCell {
    
    //var buttonImageURL : String?
    var carModelTitle : String?
    var buttonImage : UIImage?
    var distanceText : String?
    var batteryPercentageText : String?
    
    
    // country name text view
    var carModelName : UITextView = {
        
        var countryNameText = UITextView()
        countryNameText.translatesAutoresizingMaskIntoConstraints = false
        countryNameText.isEditable = false
        countryNameText.isSelectable = false
        countryNameText.isUserInteractionEnabled = false
        countryNameText.isScrollEnabled = false
        countryNameText.backgroundColor = nil
        countryNameText.font = UIFont.systemFont(ofSize: 15)
        return countryNameText
        
    }()
    // distance text view
    var distance : UITextView = {
        
        var distanceText = UITextView()
        distanceText.translatesAutoresizingMaskIntoConstraints = false
        distanceText.isEditable = false
        distanceText.isSelectable = false
        distanceText.isUserInteractionEnabled = false
        distanceText.isScrollEnabled = false
        distanceText.backgroundColor = nil
        distanceText.font = UIFont.systemFont(ofSize: 15)
        return distanceText
    }()
    
    // distance text view
    var battery : UITextView = {
        
        var batteryPercentageText = UITextView()
        batteryPercentageText.translatesAutoresizingMaskIntoConstraints = false
        batteryPercentageText.isEditable = false
        batteryPercentageText.isSelectable = false
        batteryPercentageText.isUserInteractionEnabled = false
        batteryPercentageText.isScrollEnabled = false
        batteryPercentageText.backgroundColor = nil
        batteryPercentageText.font = UIFont.systemFont(ofSize: 15)
        return batteryPercentageText
    }()
    
    // image view
    var buttonImageView : UIImageView = {
        
        var buttonImageView = UIImageView()
        buttonImageView.translatesAutoresizingMaskIntoConstraints = false
        return buttonImageView
    }()
    
    // constraints
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let maxWidthContainer: CGFloat = 100
        let maxHeightContainer: CGFloat = 50
        
        super.addSubview(carModelName)
        super.addSubview(buttonImageView)
        super.addSubview(distance)
        super.addSubview(battery)
        
        
        carModelName.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        // a lot of warnings because of constraints below..need more time to adjust
        
        buttonImageView.snp.makeConstraints { (make) in
            // centered X and Y
            make.centerY.equalToSuperview()
            
            // left anchor to the left of separatorInset
            make.left.equalTo(super.separatorInset.left)
            
            // leading and top >= 20
            make.leading.top.greaterThanOrEqualTo(20)
            
            // trailing and bottom <= 20
            make.bottom.lessThanOrEqualTo(20)
            
            // width ratio to height
            make.width.equalTo(buttonImageView.snp.height).multipliedBy(maxWidthContainer/maxHeightContainer)
            
            // this will make it as tall and wide as possible, until it violates another constraint
            make.width.height.equalToSuperview().priority(.high)
            
            // max height
            make.height.lessThanOrEqualTo(maxHeightContainer)
            
        }
        
        distance.snp.makeConstraints { (make) in
            
            make.centerY.equalToSuperview()
            make.right.greaterThanOrEqualToSuperview()
            //make.rightMargin.lessThanOrEqualTo(super.separatorInset.right)

        }
        
        battery.snp.makeConstraints { (make) in
            make.right.equalToSuperview()
        }
        
        
    }
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        if let buttonTitle = carModelTitle {
            carModelName.text = buttonTitle
        }
        
        if let buttonImage = buttonImage {
            buttonImageView.image = buttonImage
        }
        if let distanceText = distanceText {
            distance.text = String(distanceText)
        }
        if let batteryPercentageText = batteryPercentageText {
            battery.text = String(batteryPercentageText)
        }
        
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


