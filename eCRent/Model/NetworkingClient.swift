//
//  NetworkingClient.swift
//  eCRent
//
//  Created by Laurynas Kapacinskas on 2020-10-07.
//
import UIKit
import Alamofire
import AlamofireImage

protocol NetworkingClientDelegate {
    func fetchCarData (_ networkingClient : NetworkingClient, cars : [CarModel])
    func didFailWithError (error : Error)
}

struct NetworkingClient {
    //fetching data
    var networkingDelegate : NetworkingClientDelegate?
    
    func fetchCarData() {
        
        let urlString = "https://development.espark.lt/api/mobile/public/availablecars"
        
        if let url = URL(string: urlString){
            
            AF.request(url)
                .responseData
                { response in
                    
                    if case .success(let data) = response.result {
                        if let safeData =  self.parseJSON(data) {
                            networkingDelegate?.fetchCarData(self, cars: safeData)
                        }
                    }
                    else if response.error != nil {
                        networkingDelegate?.didFailWithError(error: response.error!)
                        
                    }
                }
        }
    }
    
    
    //parsing  data
    func parseJSON (_ carsData : Data) -> [CarModel]? {
        
        let decoder = JSONDecoder()
        do {
            let decodedData = try decoder.decode([CarData].self, from: carsData)
            var cars = [CarModel]()
            for car in decodedData {
                let carModel = car.model.title
                let plateNumber = car.plateNumber
                let batteryPercentage = car.batteryPercentage
                let photoURL = car.model.photoUrl
                let latitude = car.location.latitude
                let longitude = car.location.longitude
                
                let car = CarModel(carModel: carModel, plateNumber: plateNumber, batteryPercentage: batteryPercentage, photo: photoURL, latitude: latitude, longitude: longitude)
                
                cars.append(car)
            }
            
            return cars
        }
        catch {
            return nil
        }
    }
    
    
}
