//
//  ResponseModel.swift
//  eCRent
//
//  Created by Laurynas Kapacinskas on 2020-10-07.
//

import Foundation

// response JSON structure
struct CarData : Decodable {
    
   // let id : Int
    let plateNumber : String
    let location : LocationInfo
    let model : ModelInfo
    let batteryPercentage : Int
    let isCharging : Bool
    
}

struct LocationInfo : Decodable {
    
   // let id : Int
    let latitude : Double
    let longitude : Double
    let address : String
    
}

struct ModelInfo : Decodable {

  //  let id : Int
    let title : String
    let photoUrl : String
}
